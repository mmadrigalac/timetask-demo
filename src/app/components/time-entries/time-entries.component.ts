import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { TimetaskService } from 'src/app/services/timetask.service';
import { element } from 'protractor';

export interface PeriodicElement {
idtime: number;
billable: string;
client: string;
clientid: number;
date: string;
description: string;
module: string;
project: string;
time: number;
worktype: string;
}


const ELEMENT_DATA: PeriodicElement[] = [
  {billable: '', client: '', date: '', description: '', module: '', project: '', time: null, clientid:0 ,worktype: '', idtime: null},
];

/**
 * @title Table with sorting
 */

@Component({
  selector: 'app-time-entries',
  templateUrl: './time-entries.component.html',
  styleUrls: ['./time-entries.component.scss']
})

export class TimeEntriesComponent implements OnInit {
  displayedColumns: string[] = ['project', 'module', 'taskName', 'workType', 'description', 'date', 'time', 'billable', 'editTime', 'delete'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  token:string;

  constructor(
     private timetaskSvc: TimetaskService
  ) { }

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {

    this.timetaskSvc.currentState.subscribe(
      (message)=>{
        if(message === 'update'){
          this.getInitialData();
        }
      }
    )

    this.dataSource.sort = this.sort;
    this.getInitialData();
  }

  /**
   * Get from the API the data required to populate the table
   */
  getInitialData() {
    let date = new Date('06/20/2020');
    // let date = new FormControl(new Date());
    // serializedDate = new FormControl((new Date()).toISOString());

    this.timetaskSvc.getTimeDetails(date).then(
      (result)=>{

        let mappedResults = result.map((timeEntry) =>{
             timeEntry.isEditable = false;
             timeEntry.billable = timeEntry.billable == "t"? true : false;
            return timeEntry;
         });

        this.dataSource = mappedResults;
      }
    );
  }

  /**
   * Call the APi to Update the selected element.
   * @param element : Element selected in the table
   */
  updateTimeDetails(element){
    element.date = this.formatDate(element.date);

    let changes ={
      billable : element.billable == true? 't' : 'f',
      description: element.description,
      date:  element.date,
      time: element.time
    }

    // In case the element has been edited, the update will be done before set editable flag to false.
    if(element.isEditable){
      this.timetaskSvc.updateTimeDetails(element.id,changes).subscribe(
        ()=>{
         console.log('success');
         this.getInitialData();
        },
        (error)=>{
          console.error(error);
        }
      )
    }

    element.isEditable = !element.isEditable;
  }

  /**
   * Set the date in a valid format for the API
   */
  private formatDate(date: any) {
    if(date instanceof Date){
      return date.toISOString().substr(0,10);
    }

    return new Date(date).toISOString().substr(0,10);
  }

  /**
   * Delete the Time entry
   * @param element time entry that will be deleted
   */
  deleteTimeDetails(element){
    this.timetaskSvc.deleteTimeDetails(element.id).subscribe(
      ()=>{
        this.getInitialData();
      }
    )
  }
}
