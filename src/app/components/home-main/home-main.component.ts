import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { UsertokenGetterComponent } from '../usertoken-getter/usertoken-getter.component';
import { TimetaskService } from 'src/app/services/timetask.service';
import { AddTimeComponent } from '../add-time/add-time.component';

@Component({
  selector: 'app-home-main',
  templateUrl: './home-main.component.html',
  styleUrls: ['./home-main.component.scss']
})
export class HomeMainComponent implements OnInit {

  token:string;

  constructor(
     public dialog: MatDialog,
     private timetaskSvc: TimetaskService
  ) { }

  ngOnInit(): void {

    let userToken = localStorage.getItem('token');

    if( !userToken ){
      // open up a dialog modal on include the token
      const dialogRef = this.dialog.open(UsertokenGetterComponent, {
        height: '230px',
        width: '255px',
      });

      // Executed when the dialog is closed.
      dialogRef.afterClosed().subscribe(result => {
        localStorage.setItem('token', result);
      });
    }
    this.timetaskSvc.setApiToken();


    /*******Parte para pruebas*********************/
    let date = new Date('06/20/2020');


    // this.timetaskSvc.getTimeDetails(date).then(
    //   (result)=>{
    //     console.log(result);
    //   }
    // );

    // this.timetaskSvc.getClient().
    // then(
    //   (response)=>{
    //     let client = response[0]

    //     console.log(response);
    //     this.timetaskSvc.getProjects(client.id).then(
    //       (response)=>{
    //         let project = response[3];
    //         console.log(response)
    //         this.timetaskSvc.getProjectOptions(project.id).subscribe(
    //           (response)=>{
    //             console.log(response)
    //           }
    //         )
    //       }
    //     )
    //   }
    // )

  /************************************************** */
  }

  addTime(){
    const dialogRef = this.dialog.open(AddTimeComponent, {
      width: '600px',
      height: '600px',
    });

    dialogRef.afterClosed().subscribe(
      result => {
        this.timetaskSvc.changeState("update");
      }
    );
  }

}
