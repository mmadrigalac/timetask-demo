import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { TimetaskService } from 'src/app/services/timetask.service';

@Component({
  selector: 'app-add-time',
  templateUrl: './add-time.component.html',
  styleUrls: ['./add-time.component.scss']
})
export class AddTimeComponent implements OnInit {

  timeDetails: FormGroup;
  projectDetails: FormGroup;
  isEditable = false;
  loadingOptions = false;

  clients: any = [];
  projects: any = [];
  modules: any =[];
  tasks: any =[];
  worktypes: any =[];


  constructor(
    private timetask: TimetaskService,
    private _formBuilder: FormBuilder
  )
  {
    timetask.getClient()
    .then(
      result => {
        this.clients = result;
      }
    )
  }

  ngOnInit() {
    this.timeDetails = this._formBuilder.group({
      time: ['', Validators.required],
      description: ['',Validators.required],
      date: [Date.now(),Validators.required],
      client: ['',Validators.required]
    });
    this.projectDetails = this._formBuilder.group({
      projectid: ['', Validators.required],
      moduleid: ['', Validators.required],
      taskid: [''],
      worktypeid: ['', Validators.required],
      billable: [false],
    });
  }

  /**
   * Get the list of projects using the selected Client
   */
  getProjectList(){
    let client = this.timeDetails.get('client').value;

    this.timetask.getProjects(client)
    .then(
      (results) =>{
        this.projects = results;
        console.log(results);
      }
    );
  }

  /**
   * Get The task, Module and worktypes based on the selected project
   */
  getProjectOptions(){
    let projectId = this.projectDetails.get('projectid').value;
    this.loadingOptions = true;

    this.timetask.getProjectOptions(projectId)
    .subscribe(
      (results)=>{

        this.modules = results[0];
        this.tasks = results[1];
        this.worktypes = results[2];

        // Set the loading flag to false to allow selection
        this.loadingOptions = false;
      }
    )
  }

  /**
   * Creates a new time Entry
   */
  postNewTimeEntry(){

    this.loadingOptions = true;
    let timeEntry = Object.assign({}, this.timeDetails.value, this.projectDetails.value);

    this.timetask.createTimeDetail(timeEntry).subscribe(
       (result)=>{
         console.log(result);
         this.loadingOptions = false;
       }
     );
  }
}
