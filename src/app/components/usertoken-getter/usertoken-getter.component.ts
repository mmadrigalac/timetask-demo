import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usertoken-getter',
  templateUrl: './usertoken-getter.component.html',
  styleUrls: ['./usertoken-getter.component.scss']
})
export class UsertokenGetterComponent implements OnInit {

  token="";
  constructor() { }

  ngOnInit(): void {
  }

}
