import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsertokenGetterComponent } from './usertoken-getter.component';

describe('UsertokenGetterComponent', () => {
  let component: UsertokenGetterComponent;
  let fixture: ComponentFixture<UsertokenGetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsertokenGetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsertokenGetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
