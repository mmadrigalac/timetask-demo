import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialsModule } from './materials/materials.module';
import { RestangularModule } from 'ngx-restangular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeMainComponent } from './components/home-main/home-main.component';
import { UsertokenGetterComponent } from './components/usertoken-getter/usertoken-getter.component';
import { TimeEntriesComponent } from './components/time-entries/time-entries.component';
import { AddTimeComponent } from './components/add-time/add-time.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeMainComponent,
    UsertokenGetterComponent,
    TimeEntriesComponent,
    AddTimeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialsModule,
    FormsModule,
    ReactiveFormsModule,
    RestangularModule.forRoot( RestangularConfigFactory)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function RestangularConfigFactory(RestangularProvider){
  RestangularProvider.setBaseUrl('https://api.myintervals.com/');
}
