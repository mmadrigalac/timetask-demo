import { Injectable } from '@angular/core';
import { Restangular } from "ngx-restangular";
import { forkJoin, BehaviorSubject } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class TimetaskService {

  private token:string;
  private authHeader: any;
  private time : any;
  private personalInfo: any;

  private state = new BehaviorSubject('idle');
  currentState = this.state.asObservable();

   constructor(
    private restAngular: Restangular,
  )
  {
    this.time = this.restAngular.one('time');
   }

  /**
   * Set the token ready to use on localStorage.
   */
  setApiToken(){

    // token was aready converted for use
    if(this.token){
      return this.token;
    }

    let token = localStorage.getItem('token');
    // Add albitrary string to the token as API requires.
    token += ":password";

    // Base64 Encoding.
    this.token = btoa(token);

    this.authHeader = {
      Authorization:`Basic ${this.token}`
    }

    this.getPersonalInfo();
  }

  /**
   * Returns the timetask entries based on the date range provided
   * @param dateBegin :Date
   * @param dateEnd :Date
   */
  getTimeDetails<promise>(dateBegin: Date, dateEnd?: Date){
    let dateParams = {
      datebegin: dateBegin.toISOString().substr(0,10),
      dateend: dateEnd? dateEnd.toISOString().substr(0,10) : new Date().toISOString().substr(0,10)
    }

    return this.time.get(dateParams, this.authHeader).toPromise()
    .then(
      result => result.time
    );
  }

  /**
   * Update details of a time Entry.
   * @param details : correspond to the properties that requires the change.
   */
  updateTimeDetails<observable>( timeid:number, details: any){
    return this.time.customPUT(details, timeid, {}, this.authHeader);
  }

  /**
   * Delete a time entry
   * @param id : identifier of time entry.
   */
  deleteTimeDetails<observable>( id: string){
    return this.time.one(id).remove({},this.authHeader);
  }

  /**
   * Creates a new entry with the details included
   * @param timeDetail time Details to create a new entry.
   */
  createTimeDetail<observable>( timeDetail:any ){
    let timeEntry = Object.assign({},timeDetail,{personid: this.personalInfo.id} );

    // Mapping and fixing object before send it to the API
    timeEntry.date = timeEntry.date.toISOString().substr(0,10);
    timeEntry.billable = timeEntry.billable? 't' :'f';
    delete timeEntry.client;

    console.log(timeEntry);
    return this.time.post('',timeEntry,{},this.authHeader);
  }

  /*******Information Getters*********/

  /**
   * Get Personal Data and store it on service variables
   */
  getPersonalInfo(){
    this.restAngular.one('me').get({},this.authHeader).toPromise()
    .then(
      (result)=> this.personalInfo = result.me[0]
    );
  }

  /**
   * Get Client list linked to the current User doing the request
   */
  getClient(){
    return this.restAngular.one('client').get({},this.authHeader).toPromise()
    .then(
      (result)=> result.client
    );
  }

  /**
   * Get Projects based on the ClientID provided.
   * @param clientid: Integer - Client Identification number
   */
  getProjects( clientid: number){
    return this.restAngular.one('project').get({clientid},this.authHeader).toPromise()
    .then(
      (result)=> result.project
    );
  }

  /**
   * Get the Modules related to the project selected.
   */
  getModules( projectid: number){
    return this.restAngular.one('projectmodule').get({projectid},this.authHeader).toPromise()
    .then(
      (result)=> result.projectmodule
    );
  }

  /**
   * Get the task assigned to the project selected
   */
  getTask(projectid:number){
    return this.restAngular.one('task').get({projectid},this.authHeader).toPromise()
    .then(
      (result)=> result.task
    );
  }

  /**
   * Get the work types available for project and person
   */
  getWorktype(projectid:number){
    let personid = this.personalInfo.id;

    return this.restAngular.one('projectworktype').get({projectid, personid},this.authHeader).toPromise()
    .then(
      (result)=> result.projectworktype
    );
  }

  /**
   * Compendium of request related to project selection
   * @param projectID
   */
  getProjectOptions( projectID: number){

    return forkJoin([
      this.getModules(projectID),
      this.getTask(projectID),
      this.getWorktype(projectID)
    ])
  }

  /**
   * Call the observable to notify other elements there is a change in the data.
   * @param state refering to the current status of the data
   */
  changeState(state: string){
    this.state.next(state);
  }

}
