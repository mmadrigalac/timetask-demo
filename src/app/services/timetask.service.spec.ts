import { TestBed } from '@angular/core/testing';

import { TimetaskService } from './timetask.service';

describe('TimetaskService', () => {
  let service: TimetaskService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TimetaskService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
